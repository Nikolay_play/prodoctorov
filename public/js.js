"use strict";
async function getResponse() {
    let response = await fetch(`https://json.medrating.org/photos?albumId=1`)
    let response_two = await fetch('https://json.medrating.org/users/')
    let response_free = await fetch(`https://json.medrating.org/albums?userId=1`)
    let photo = await response.json()
    let users = await response_two.json()
    let albums = await response_free.json()
    photo = photo.splice(0, 10)
    users = users.splice(0, 10)
    albums = albums.splice(0, 10)


    let list = document.querySelector('.posts')
    for (let i in photo) {
        list.innerHTML += `
            
            <ul>
                <li>
                    ${users[i].name}
                </li>
                <ul>
                    <li> 
                    ${albums[i].title}
                    </li>
                    
                <ul>
                    <li>
                    <input id="user-favorite" class="user-checkbox  ls" type="checkbox"  name="user-favorite">
                    <svg class="user-favorite-icon" width="28" height="28" viewBox="0 0 28 28">
                    <path d="M14 21l-8.22899 4.3262 1.57159-9.1631L.685209 9.67376 9.8855 8.33688 14 0l4.1145 8.33688 9.2003 1.33688-6.6574 6.48934 1.5716 9.1631L14 21z"/>
                    </svg>
                    <a href="${photo[i].url}" class="toggle_img">
                    <div class="photo" data-title="${photo[i].title}"><img src = "${photo[i].url}" width = "100"></div>
                    </a>
        
                </li>
                </ul>
                </ul>

            </ul>`
    }
    localStorage.setItem("photo", JSON.stringify(photo));
    localStorage.setItem("users", JSON.stringify(users));
    localStorage.setItem("albums", JSON.stringify(albums));
}
getResponse()


$(document).ready(function() {
    $('.toggle_img').on('click', function() {
        var $this = $(this);
        var currentSrc = this.href;
        var toggleSrc = $this.data("img");
        var img = $this.find('img')[0];
        img.src = toggleSrc;
        this.href = toggleSrc;
        $this.data("img", currentSrc);
        return false;
    });
});
