
for (let li of tree.querySelectorAll('li')) {
    let span = document.createElement('span'); /* создание span */
    span.classList.add('show'); /* добавление класса для span */
    li.prepend(span); /* вставить span внутри li */
    span.append(span.nextSibling) /* присоединить к span следующий узел */
}

tree.onclick = function(event) {
    console.log(event.target.tagName);
    if (event.target.tagName != 'SPAN') return;

    let child = event.target.parentNode.querySelector('ul');

    if (!child) return;

    child.hidden = !child.hidden;

    if (child.hidden) {
        event.target.classList.add('hide');
        event.target.classList.remove('show');
    } else {
        event.target.classList.add('show');
        event.target.classList.remove('hide');
    }
}
